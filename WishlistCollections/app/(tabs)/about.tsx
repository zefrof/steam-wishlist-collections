import { Text, View, StyleSheet } from 'react-native';

import { Providers } from "@/components/Providers";

export default function AboutScreen() {
  return (
    <Providers>
      <View style={styles.container}>
        <Text style={styles.text}>About screen</Text>
      </View>
    </Providers>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
  },
});
