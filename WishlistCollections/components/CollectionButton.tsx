import { StyleSheet, Text, View } from "react-native";

type Props = {
  id: String;
  colName: String; 
};

export default function CollectionButton({ id, colName }: Props) {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{colName}</Text>
      <Text style={styles.text}>{id}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 320,
    height: 68,
    padding: 10,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#464C55',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
  },
});
