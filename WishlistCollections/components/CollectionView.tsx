import { View, StyleSheet } from 'react-native';
import { useContext, useState } from 'react';

import CollectionButton from "@/components/CollectionButton";
import CreateCollectionButton from "@/components/CreateCollectionButton";
import CreateCollectionModal from "@/components/CreateCollectionModal";
import { CollectionsContext } from "@/components/Providers";

export default function CollectionView() {
  const collectionsContext = useContext(CollectionsContext);
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const onModalOpen = () => {
    setIsModalVisible(true);
  };
  
    const onModalClose = () => {
    setIsModalVisible(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.colContainer}>
        {collectionsContext?.collections.map(element => (
          <CollectionButton id={element.id} colName={element.label} />
        ))}
      </View>
      <CreateCollectionButton onPress={onModalOpen} />
      <CreateCollectionModal isVisible={isModalVisible} onClose={onModalClose} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
  },
  colContainer: {
  	margin: 50,
  	flexDirection: 'row',
  	flexWrap: 'wrap',
  	justifyContent: 'center',
  	alignItems: 'center',
  },
});
