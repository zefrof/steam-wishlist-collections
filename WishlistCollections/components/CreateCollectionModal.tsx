import { Modal, View, Text, TextInput, Pressable, StyleSheet } from 'react-native';
import { PropsWithChildren, useContext, useState } from 'react';
import MaterialIcons from '@expo/vector-icons/MaterialIcons';

import { CollectionsContext } from "@/components/Providers";

type Props = PropsWithChildren<{
  isVisible: boolean;
  onClose: () => void;
}>;

export default function CreateCollectionModal({ isVisible, onClose }: Props) {
  const collectionsContext = useContext(CollectionsContext);
  const [text, onChangeText] = useState('');

  const newCollection = () => {
    // addCollection([...collections, trimmedText]);
    collectionsContext?.saveCollection(text);
    onChangeText("");
    onClose();
  };
  
  return (
    <Modal animationType="fade" transparent={true} visible={isVisible}>
      <View style={styles.modalContent}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>New Collection</Text>
          <Pressable onPress={onClose}>
            <MaterialIcons name="close" color="#fff" size={22} />
          </Pressable>
        </View>
        <TextInput style={styles.input} autoCapitalize={"characters"} onChangeText={onChangeText} value={text} />
        <Pressable style={[styles.button, { backgroundColor: '#464C55' }]} onPress={newCollection}>
          <Text style={[styles.buttonLabel, { color: '#fff' }]}>Create Collection</Text>
        </Pressable>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    height: '40%',
    width: '100%',
    backgroundColor: '#25292e',
    borderTopRightRadius: 18,
    borderTopLeftRadius: 18,
    position: 'absolute',
    bottom: 0,
  },
  titleContainer: {
    height: '16%',
    backgroundColor: '#464C55',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    color: '#fff',
    fontSize: 16,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    color: '#fff',
  },
  button: {
    borderRadius: 10,
    width: 320,
    height: 68,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonLabel: {
    color: '#fff',
    fontSize: 16,
  },
});
