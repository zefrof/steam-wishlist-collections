import { View, StyleSheet } from 'react-native';
import { useContext, useState } from 'react';

import GameComponent from "@/components/GameComponent";

export default function WishlistView() {
  const [wishlist, setWishlist] = useState<Object>({
    "254320":{"name":"Duskers","capsule":"https:\/\/shared.fastly.steamstatic.com\/store_item_assets\/steam\/apps\/254320\/header_292x136.jpg?t=1671221115","review_score":8,"review_desc":"Very Positive","reviews_total":"2,127","reviews_percent":90,"release_date":"1463590680","release_string":"May 18, 2016","platform_icons":"<span class=\"platform_img win\"><\/span><span class=\"platform_img mac\"><\/span><span class=\"platform_img linux\"><\/span>","subs":[{"packageid":31212,"bundleid":null,"discount_block":"<div class=\"discount_block discount_block_large no_discount\" data-price-final=\"1999\" data-bundlediscount=\"0\" data-discount=\"0\"><div class=\"discount_prices\"><div class=\"discount_final_price\">$19.99<\/div><\/div><\/div>","discount_pct":null,"price":"1999","is_hardware":null}],"type":"Game","screenshots":["ss_fd7b606082fc64b9ac575635204a7ee5307cac72.jpg","ss_a3372b4d0db1b23b05977dc3f391f8776913afa9.jpg","ss_539846a871ea2cb8fe9291b07a1d278661d8183b.jpg","ss_92a46c245f5d38e5ac654d50c6deff0153d2d0da.jpg"],"review_css":"positive","priority":0,"added":1663731369,"background":"https:\/\/shared.fastly.steamstatic.com\/store_item_assets\/steam\/apps\/254320\/page_bg_generated_v6b.jpg?t=1671221115","rank":983,"tags":["Exploration","Typing","Real Time Tactics","Space","Roguelite"],"is_free_game":false,"deck_compat":"2","win":1,"mac":1,"linux":1},
    "268910":{"name":"Cuphead","capsule":"https:\/\/shared.fastly.steamstatic.com\/store_item_assets\/steam\/apps\/268910\/header_292x136.jpg?t=1709068852","review_score":9,"review_desc":"Overwhelmingly Positive","reviews_total":"153,169","reviews_percent":96,"release_date":"1506693600","release_string":"Sep 29, 2017","platform_icons":"<span class=\"platform_img win\"><\/span><span class=\"platform_img mac\"><\/span>","subs":[{"packageid":35659,"bundleid":null,"discount_block":"<div class=\"discount_block discount_block_large no_discount\" data-price-final=\"1999\" data-bundlediscount=\"0\" data-discount=\"0\"><div class=\"discount_prices\"><div class=\"discount_final_price\">$19.99<\/div><\/div><\/div>","discount_pct":null,"price":"1999","is_hardware":null}],"type":"Game","screenshots":["ss_e3096a5555cb77d88db165c83d5ef3a24af1354a.jpg","ss_615455299355eaf552c638c7ea5b24a8b46e02dd.jpg","ss_483fb089be0093beeef03525276803a9ca4f66a1.jpg","ss_48477e4a865827aa0be6a44f00944d8d2a3e5eb9.jpg"],"review_css":"positive","priority":0,"added":1656526019,"background":"https:\/\/shared.fastly.steamstatic.com\/store_item_assets\/steam\/apps\/254320\/page_bg_generated_v6b.jpg?t=1671221115","rank":842,"tags":["Difficult","Cartoon","Co-op","Platformer","Great Soundtrack"],"is_free_game":false,"deck_compat":"3","win":1,"mac":1}
  });

  return (
    <View style={styles.container}>
      {Object.entries(wishlist).map(([key, value]) => (
        <GameComponent id={key} value={value} />
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
  },
  button: {
    fontSize: 20,
    textDecorationLine: 'underline',
    color: '#fff',
  },
});
