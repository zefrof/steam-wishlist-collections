import { Pressable, StyleSheet } from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';

type Props = {
	onPress: () => void;
}

export default function CreateCollectionButton({onPress}: Props) {
  return (
    <Pressable style={[styles.button, { backgroundColor: '#464C55' }]} onPress={onPress}>
      <Ionicons name='add-outline' color='#fff' size={24} />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    width: 320,
    height: 68,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
