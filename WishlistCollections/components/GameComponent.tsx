import { StyleSheet, Text, View } from "react-native";
import { useContext } from "react";

import ImageViewer from "@/components/ImageViewer";
import MultiSelectComponent from "@/components/MultiSelect";

type Props = {
  id: String,
  value: Object,
};

export default function GameComponent({ id, value }: Props) {

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{value.name}</Text>
      <View style={styles.imageContainer}>
        <ImageViewer imgSource={value.capsule} />
      </View>
      <View>
        <Text style={styles.text}>
          Overall reviews: {value.review_desc}{'\n'}
          Release date: {value.release_string}{'\n'}
        </Text>
        <MultiSelectComponent />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 320,
    height: 300,
    padding: 10,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#464C55',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
  },
  imageContainer: {
    flex: 1,
  },
});
