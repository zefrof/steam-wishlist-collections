import { createContext, useState } from 'react';

interface ICollectionsContextType {
    id: String,
    label: String,
}

type CollectionsContextType = {
    collections: ICollectionsContextType[];
    saveCollection: (label: String) => void;
}

type Props = {
    children: any,
}

// TODO: remove null and provide default
export const CollectionsContext = createContext<CollectionsContextType | null>(null);

export function Providers({ children }: Props) {
    const [collections, setCollections] = useState<ICollectionsContextType[]>([
        {id: "1", label: "Default"},
        {id: "2", label: "10$"},
    ]);

    const saveCollection = (label: String) => {
        const trimmedText: String = label.trim();
        // TODO: don't allow duplicate values
        /* if (collections.includes(trimmedText)) {
            return;
        } */
        const newCollection = {
            id: Math.random().toString(), //TODO: do better
            label: trimmedText,
        }
        setCollections([...collections, newCollection]);
    }

    return (
        <CollectionsContext.Provider value={{collections, saveCollection}} >
            {children}
        </CollectionsContext.Provider>
    )
}
