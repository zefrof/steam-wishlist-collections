use axum::{
    body::Body,
    extract::Path,
    http::{Request, StatusCode},
    Json,
    response::{Html, IntoResponse, Redirect},
    routing::{get, post},
    Router,
};
// use serde::{Deserialize, Serialize};
use serde_json::{Value};
use std::fmt::Write;
use steam_openid::SteamOpenId;
use urlencoding::encode;

#[tokio::main]
async fn main() {

    // build our application with a route
    let app = Router::new()
        .route("/", get(root))
        .route("/login", get(login))
        .route("/callback", get(callback))
        .route("/wishlist", get(wishlist));

    // run our app with hyper, listening globally on port 8082
    let listener = tokio::net::TcpListener::bind("0.0.0.0:8082").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

async fn root() -> impl IntoResponse {
    Html(
        r#"
            <!DOCTYPE html>
            <html lang="en">
            <body>
                <a href="/login">
                    <img src="https://community.fastly.steamstatic.com/public/images/signinthroughsteam/sits_02.png" alt="steam login button">
                </a>
            </body>
            <footer>This project is not affiliated with Steam or Valve.</footer>
        "#,
    )
}

async fn login() -> impl IntoResponse {
    let openid = SteamOpenId::new("https://dev2.dolantube.com", "/callback").expect("Creating SteamOpenId failed");
	
	// Redirect the user to this url:
	let redirect_url: String = openid.get_redirect_url().to_string();

    return Redirect::to(redirect_url.as_str()).into_response();
}

async fn callback(req: Request<Body>) -> impl IntoResponse {
    let openid = SteamOpenId::new("https://dev2.dolantube.com", "/callback").expect("Creating SteamOpenId failed");

    // println!("{}", req.uri().query().unwrap());
	let steamid64 = openid.verify(req.uri().query().unwrap());
	let id = steamid64.await.expect("await-ing for the steamid64 failed").to_string();

    let mut redirect_url: String = "https://dev.dolantube.com/?id=".to_string();
	redirect_url.push_str(&id);
    // TODO: fetch wishlist here and send it to UI, is this even possible/feasible?
    return Redirect::to(redirect_url.as_str()).into_response();
}

async fn wishlist() -> String {
    let base_url: String = "https://api.steampowered.com/".to_string();
    // TODO: replace clone if there's a need for speed
    let mut wishlist_url: String = base_url.clone();
    wishlist_url.push_str("IWishlistService/GetWishlist/v1/?steamid=76561198022874885");

    let wishlist_body: String = reqwest::get(wishlist_url.as_str())
        .await.expect("await-ing HTTP request failed").text()
        .await.expect("await-ing text failed");

    let wishlist: Value = serde_json::from_str(&wishlist_body).expect("Unable to convert body string to json");

    let mut appids: String = "".to_string();
    for value in wishlist["response"]["items"].as_array().expect("Couldn't convert to array") {
        write!(appids, "{{\"appid\": {}}},", value["appid"]).unwrap();
    }
    // Remove trailing comma
    appids.pop();

    // TODO?: Convert to serde Value and use as_str()
    let mut params: String = "{\"ids\":[".to_string();
    params.push_str(appids.as_str());
    params.push_str("],
        \"context\": {
            \"language\": \"english\",
            \"country_code\": \"US\",
            \"steam_realm\": 1
        },
        \"data_request\":{
            \"include_release\": true,
            \"include_basic_info\": true
        }
    }");
    let encoded_params = encode(params.as_str());

    let mut items_url: String = base_url;
    items_url.push_str("IStoreBrowseService/GetItems/v1/?input_json=");
    items_url.push_str(&encoded_params);

    let body: String = reqwest::get(items_url.as_str()).await.expect("await-ing HTTP request failed")
        .text().await.expect("await-ing text failed");

    return body;
}
